package hr.ferit.katicilan.GymFriend

data class Exercise(
    val image: Int = 0,
    val description: String = "",
    var isFavorited: Boolean = false,
)

data class MuscleGroup(
    val title: String = "",
    val exercises: List<Exercise> = listOf(),
)

