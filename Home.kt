package hr.ferit.katicilan.GymFriend

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.Home
import androidx.compose.material.icons.filled.Info
import androidx.compose.material.icons.filled.ShoppingCart
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController


@Composable
fun HomeScreen(navController: NavController) {

    LazyColumn(
        verticalArrangement = Arrangement.Top,
        modifier = Modifier
            .fillMaxSize()
            .background(color = Color.Black)
            .padding(bottom = 60.dp)
    ) {
        item {
            ScreenTitle(
                title = "GymFriend",
                subtitle = "Have a great workout"
            )
            Text(
                text = "GymFriend is app, that can help you with exercise choice " +
                        "if you are clueless and you don't know what to do today. " +
                        "Here you can find a lot of different exercise. All you must " +
                        "do is just click on your group of muscles and it will guide you " +
                        "on screen with plenty of workouts...",
                fontSize = 15.sp,
                color = Color.Gray,
                modifier = Modifier.padding(10.dp)
            )

            MuscleGroup(
                R.drawable.chest,
                R.drawable.back,
                "Chest",
                "Back",
                navController,
                "chestScreen",
                "backScreen"
            )
            MuscleGroup(
                R.drawable.arms,
                R.drawable.legs,
                "Arms",
                "Legs",
                navController,
                "armScreen",
                "legScreen"
            )
            MuscleGroup(
                R.drawable.cardio,
                R.drawable.plank,
                "Cardio",
                "Bodyweight",
                navController,
                "cardioScreen",
                "bdwtScreen"
            )
        }
    }
    NavigationBar(navController = navController)
}
@Composable
fun ScreenTitle(
    title: String,
    subtitle: String
) {
    Box(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = Color.DarkGray)
            .height(100.dp)
    ) {
        Text(
            text = subtitle,
            style = TextStyle(color = Color.Gray, fontSize = 12.sp,
                fontWeight = FontWeight.Light, fontStyle = FontStyle.Italic),
            modifier = Modifier
                .padding(horizontal = 15.dp, vertical = 5.dp)
        )
        Text(
            text = title,
            style = TextStyle(color = Color.Black, fontSize = 50.sp,
                fontWeight = FontWeight.Bold),
            modifier = Modifier
                .align(Alignment.Center)
                .padding(vertical = 16.dp)
        )
    }
}

@Composable
fun TabButton(
    icon: ImageVector,
    onClick: () -> Unit
) {
    Button(
        elevation = null,
        colors = ButtonDefaults.buttonColors(Color.DarkGray),
        modifier = Modifier.fillMaxHeight(),
        onClick = { onClick() }
    ) {
        Icon(
            imageVector = icon,
            contentDescription = null,
            modifier = Modifier
                .size(85.dp)
        )
    }
}

@Composable
fun NavigationBar(navController: NavController) {
    Box(
        modifier = Modifier.fillMaxSize()

    ) {
        Row(
            verticalAlignment = Alignment.Bottom,
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .background(Color.DarkGray)
                .fillMaxWidth()
                .height(60.dp)

        ) {
            TabButton(icon = Icons.Default.Home){
                navController.navigate("home")
            }
            TabButton(icon = Icons.Default.Favorite) {
                navController.navigate("favorite")
            }
            TabButton(icon = Icons.Default.Info) {navController.navigate("info")}
        }
    }
}

@Composable
fun MuscleGroup(
    imageResource: Int,
    imageResource1: Int,
    title: String,
    title1: String,
    navController: NavController,
    destination: String,
    destination1: String
) {
    Row(modifier = Modifier.fillMaxWidth()) {

    Box(
        modifier = Modifier
            .padding(20.dp)
            .height(200.dp)
            .width(150.dp)
            .clickable { navController.navigate(destination) }
    ) {
        Image(
            painter = painterResource(id = imageResource),
            contentDescription = title,
            contentScale = ContentScale.Crop,
            modifier = Modifier.clip(RoundedCornerShape(16.dp)),

        )
        Box(
            modifier = Modifier
                .fillMaxSize(),
        ) {
            Text(
                text = title,
                style = TextStyle(
                    color = Color.White,
                    fontWeight = FontWeight.SemiBold,
                    fontSize = 25.sp,
                    textAlign = TextAlign.Center
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .align(Alignment.Center)
            )
        }
    }
        Box(
            modifier = Modifier
                .padding(15.dp)
                .height(200.dp)
                .width(150.dp)
                .clickable {navController.navigate(destination1)}
        ) {
            Image(
                painter = painterResource(id = imageResource1),
                contentDescription = title,
                contentScale = ContentScale.Crop,
                modifier = Modifier.clip(RoundedCornerShape(16.dp))
            )
            Box(
                modifier = Modifier
                    .fillMaxSize()
            ) {
                Text(
                    text = title1,
                    style = TextStyle(
                        color = Color.White,
                        fontWeight = FontWeight.SemiBold,
                        fontSize = 25.sp,
                        textAlign = TextAlign.Center
                    ),
                    modifier = Modifier
                        .fillMaxWidth()
                        .align(Alignment.Center)
                )
            }
        }
}
}







