package hr.ferit.katicilan.GymFriend

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable

@Composable
fun AppNavigation(navController: NavHostController) {
    NavHost(
        navController = navController,
        startDestination = "home"
    ) {
        composable("home") {
            HomeScreen(navController)
        }
        composable("chestScreen") {
            ChestScreen(navController)
        }
        composable("backScreen") {
            BackScreen(navController)
        }
        composable("legScreen") {
            LegScreen(navController)
        }
        composable("favorite"){
            val favoriteExercises: List<Exercise> = allExercises.flatMap { muscleGroup ->
                muscleGroup.exercises.filter { it.isFavorited }
            }
            FavoriteScreen(navController, favoriteExercises)
        }
        composable("cardioScreen"){
            CardioScreen(navController)
        }
        composable("bdwtScreen"){
            BodyweightScreen(navController)
        }
        composable("armScreen"){
            ArmScreen(navController)
        }
        composable("info"){
            InfoScreen(navController)
        }
    }
}


