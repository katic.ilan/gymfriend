package hr.ferit.katicilan.GymFriend

import androidx.compose.foundation.Image
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Favorite
import androidx.compose.material.icons.filled.FavoriteBorder
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController


@Composable
fun MuscleGroupScreen(muscleGroup: MuscleGroup, navController: NavController) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .background(color = Color.Black)
            .padding(bottom = 60.dp)
    ) {
        ScreenTitle(
            title = muscleGroup.title,
            subtitle = "Have a great workout"
        )
        LazyColumn(
            verticalArrangement = Arrangement.Top,
            modifier = Modifier.weight(1f)
        ) {
            items(muscleGroup.exercises) { exercise ->
                ExerciseItem(
                    exercise = exercise,
                ) { exercise ->
                    exercise.isFavorited = !exercise.isFavorited
                }
            }
            }
    }
    NavigationBar(navController = navController)
}

@Composable
fun ExerciseItem(
    exercise: Exercise,
    onToggleFavorite: (Exercise) -> Unit
) {
    Row(modifier = Modifier.fillMaxHeight()) {
        Box(
            modifier = Modifier
                .padding(20.dp)
                .height(200.dp)
                .width(150.dp)
                .clickable { onToggleFavorite(exercise) }
        ) {
            Image(
                painter = painterResource(id = exercise.image),
                contentDescription = null,
                contentScale = ContentScale.Crop,
                modifier = Modifier.clip(RoundedCornerShape(16.dp))
            )
            Box(
                modifier = Modifier
                    .padding(5.dp)
                    .size(40.dp)
                    .clickable { onToggleFavorite(exercise) }
            ) {
                Icon(
                    imageVector = if (exercise.isFavorited) Icons.Default.Favorite else Icons.Default.FavoriteBorder,
                    contentDescription = null,
                    modifier = Modifier.size(40.dp)
                )
            }
        }
        Text(
            text = exercise.description,
            style = TextStyle(fontSize = 15.sp, color = Color.Gray),
            modifier = Modifier
                .padding(15.dp)
                .fillMaxSize()
                .align(Alignment.CenterVertically)
        )
    }
}
