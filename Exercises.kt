package hr.ferit.katicilan.GymFriend

val ChestExercises = listOf(
    Exercise(R.drawable.chest, "Bench Press is the best workout for hitting chest muscles" +
        ". It has 2 variation with weighted bar or with dumpbells. You just lay on the bench"+
        " and try to lift the weights above your head",true),
    Exercise(R.drawable.arms, "Biceps Curls, most common workout that comes to mind when you hear GYM" +
        " You just need some weight no matter what it is, and you just curl it with your elbow (without shoulders)",
    false),
    Exercise(R.drawable.chest, "ChestFly is great workout for chest definition, you can do it with cables " +
        "or you can do it laying down, and you need is movement as if you are flying(putting your arms together - straight)",
    false),
    Exercise(R.drawable.back, "Dips are best workout for back muscles, it hits every muscle from your shoulder" +
        " to the gluteus. All you need is high bar and you just pull up to the point that chin is above that bar",true))

val chest = MuscleGroup("Chest", ChestExercises)

val BackExercises = listOf(
    Exercise(R.drawable.chest, "Bench Press is the best workout for hitting chest muscles" +
            ". It has 2 variation with weighted bar or with dumpbells. You just lay on the bench"+
            " and try to lift the weights above your head",false),
    Exercise(R.drawable.arms, "Biceps Curls, most common workout that comes to mind when you hear GYM" +
            " You just need some weight no matter what it is, and you just curl it with your elbow (without shoulders)",
        false),
    Exercise(R.drawable.chest, "ChestFly is great workout for chest definition, you can do it with cables " +
            "or you can do it laying down, and you need is movement as if you are flying(putting your arms together - straight)",
        false),
    Exercise(R.drawable.back, "Dips are best workout for back muscles, it hits every muscle from your shoulder" +
            " to the gluteus. All you need is high bar and you just pull up to the point that chin is above that bar",false)
)

val back = MuscleGroup("Back", BackExercises)


val ArmsExercises = listOf(
    Exercise(R.drawable.chest, "Bench Press is the best workout for hitting chest muscles" +
            ". It has 2 variation with weighted bar or with dumpbells. You just lay on the bench"+
            " and try to lift the weights above your head",false),
    Exercise(R.drawable.arms, "Biceps Curls, most common workout that comes to mind when you hear GYM" +
            " You just need some weight no matter what it is, and you just curl it with your elbow (without shoulders)",
        false),
    Exercise(R.drawable.chest, "ChestFly is great workout for chest definition, you can do it with cables " +
            "or you can do it laying down, and you need is movement as if you are flying(putting your arms together - straight)",
        false),
    Exercise(R.drawable.back, "Dips are best workout for back muscles, it hits every muscle from your shoulder" +
            " to the gluteus. All you need is high bar and you just pull up to the point that chin is above that bar",false)
)

val arms = MuscleGroup("Arms", ArmsExercises)


val LegsExercises = listOf(
    Exercise(R.drawable.chest, "Bench Press is the best workout for hitting chest muscles" +
            ". It has 2 variation with weighted bar or with dumpbells. You just lay on the bench"+
            " and try to lift the weights above your head",false),
    Exercise(R.drawable.arms, "Biceps Curls, most common workout that comes to mind when you hear GYM" +
            " You just need some weight no matter what it is, and you just curl it with your elbow (without shoulders)",
        false),
    Exercise(R.drawable.chest, "ChestFly is great workout for chest definition, you can do it with cables " +
            "or you can do it laying down, and you need is movement as if you are flying(putting your arms together - straight)",
        false),
    Exercise(R.drawable.back, "Dips are best workout for back muscles, it hits every muscle from your shoulder" +
            " to the gluteus. All you need is high bar and you just pull up to the point that chin is above that bar",false)
)

val legs = MuscleGroup("Legs", LegsExercises)


val CardioExercises = listOf(
    Exercise(R.drawable.chest, "Bench Press is the best workout for hitting chest muscles" +
            ". It has 2 variation with weighted bar or with dumpbells. You just lay on the bench"+
            " and try to lift the weights above your head",false),
    Exercise(R.drawable.arms, "Biceps Curls, most common workout that comes to mind when you hear GYM" +
            " You just need some weight no matter what it is, and you just curl it with your elbow (without shoulders)",
        false),
    Exercise(R.drawable.chest, "Treadmill is great workout for chest definition, you can do it with cables " +
            "or you can do it laying down, and you need is movement as if you are flying(putting your arms together - straight)",
        true),
    Exercise(R.drawable.back, "Dips are best workout for back muscles, it hits every muscle from your shoulder" +
            " to the gluteus. All you need is high bar and you just pull up to the point that chin is above that bar",false)
)

val cardio = MuscleGroup("Cardio", CardioExercises)


val BodyweightExercises = listOf(
    Exercise(R.drawable.chest, "Bench Press is the best workout for hitting chest muscles" +
            ". It has 2 variation with weighted bar or with dumpbells. You just lay on the bench"+
            " and try to lift the weights above your head",false),
    Exercise(R.drawable.arms, "Biceps Curls, most common workout that comes to mind when you hear GYM" +
            " You just need some weight no matter what it is, and you just curl it with your elbow (without shoulders)",
        false),
    Exercise(R.drawable.chest, "ChestFly is great workout for chest definition, you can do it with cables " +
            "or you can do it laying down, and you need is movement as if you are flying(putting your arms together - straight)",
        false),
    Exercise(R.drawable.back, "Dips are best workout for back muscles, it hits every muscle from your shoulder" +
            " to the gluteus. All you need is high bar and you just pull up to the point that chin is above that bar",false)
)

val bodyweight = MuscleGroup("Bodyweight", BodyweightExercises)